/**
 * Created by Thinh on 10/25/2016.
 */

var mongoose = require('mongoose');

var airportSchema = mongoose.Schema({
    Ten:{
        type: String,
        required: true
    },
    MaSanBay:{
        type: String,
        required:true
    }
});

var FlightSchema = mongoose.Schema({
    Ma:{
        type: String,
        required: true
    },
    SanBayDi:{
        type: String,
        required:true
    },
    SanBayDen:{
        type: String,
        required: true
    },
    ThoiGianDi:{
        type: Number,
        required:true
    },
    Hang:{
        type: String,
        required: true
    },
    MucGia:{
        type: String,
        required: true
    },
    SoLuongGhe:{
        type: Number,
        required: true
    },
    GiaBan:{
        type: Number,
        required: true
    },
    GheDaDat:{
        type: Number,
        required: true
    }

});
var detailFlightSchema = mongoose.Schema({
    MaDatCho:{
        type: String,
        required: true
    },
    MaChuyenBay:{
        type: String,
        required:true
    },
    Ngay:{
        type: Number,
        required: true
    },
    Hang:{
        type: String,
        required:true
    },
    MucGia:{
        type: String,
        required: true
    },
    GiaBan:{
        type: Number,
        required: true
    }
});
var ChuyenBay = module.exports = mongoose.model('ChuyenBay', FlightSchema, 'ChuyenBay');
var ChiTietChuyenBay = module.exports = mongoose.model('ChiTietChuyenBay', detailFlightSchema, 'ChiTietChuyenBay');
var SanBay = module.exports = mongoose.model('SanBay', airportSchema, 'SanBay');


module.exports.layTatCaChuyenBay = function (callback) {
    db.collection('ChuyenBay').aggregate(callback);
};

module.exports.themChuyenBay = function (chuyenbay, callback) {
    db.collection('ChuyenBay').insert(chuyenbay, callback);
};

module.exports.capnhatChuyenBay = function (id, chuyenbay, options, callback) {
    try {
        var query = {_id: id};
        var update = {$set: {
            Ma: chuyenbay.Ma,
            SanBayDi: chuyenbay.SanBayDi,
            SanBayDen: chuyenbay.SanBayDen,
            ThoiGianDi: chuyenbay.ThoiGianDi,
            Hang: chuyenbay.Hang,
            MucGia: chuyenbay.MucGia,
            SoLuongGhe: chuyenbay.SoLuongGhe,
            GiaBan: chuyenbay.GiaBan,
            GheDaDat: chuyenbay.GheDaDat
        }};
        ChuyenBay.findOneAndUpdate(query, update, {returnNewDocument: true}, callback);
    }
    catch(err) {
        callback(err,null);
    }
};

module.exports.xoaChuyenBay = function (id, callback){
    var query = {_id: id};
    ChuyenBay.remove(query, callback);
};

module.exports.themSanbay = function (sanbay, callback) {
    db.collection('SanBay').insert(sanbay, callback);

};

module.exports.capnhatSanbay = function (masanbay, sanbay, options, callback) {

    try {
        var query = {MaSanBay: masanbay};
        var update = {
            Ten: sanbay.Ten,
            MaSanBay: sanbay.MaSanBay
        };
        SanBay.findOneAndUpdate(query, update, {returnNewDocument: true}, callback);
    }
    catch(err) {
        callback(err,null);
    }
};

module.exports.xoaSanbay = function (masanbay, callback){
    var query = {MaSanBay: masanbay};
    SanBay.remove(query, callback);
};

module.exports.capnhatThongtinHanhKhach = function (hanhkhach, options, callback) {
    try {

        var queryName = {};

        queryName["$and"]=[];
        queryName["$and"].push({ MaDatCho: hanhkhach.MaDatCho});
        queryName["$and"].push({"HanhKhach.DanhXung": hanhkhach.ThongTinCu.DanhXung});
        queryName["$and"].push({"HanhKhach.Ho": hanhkhach.ThongTinCu.Ho});
        queryName["$and"].push({"HanhKhach.Ten": hanhkhach.ThongTinCu.Ten});



        var update = {$set: {
            "HanhKhach.$.DanhXung" : hanhkhach.ThongTinMoi.DanhXung,
            "HanhKhach.$.Ho": hanhkhach.ThongTinMoi.Ho,
            "HanhKhach.$.Ten": hanhkhach.ThongTinMoi.Ten
        }
        };
        db.collection('HanhKhach').update(queryName,update, {returnNewDocument: true}, callback);

    }
    catch(err) {
        callback(err,null);
    }
};

module.exports.layHanhKhachtrenChuyenBay = function ( machuyenbay, callback) {
    var query = {MaChuyenBay: machuyenbay};
    db.collection('ChiTietChuyenBay').aggregate(
        {$match : {MaChuyenBay : machuyenbay}},
        {$lookup: {from: 'HanhKhach', localField: "MaDatCho", foreignField: "MaDatCho",as: "DanhSach"}}, callback);
};

module.exports.layDanhSachChuyenBaytuEmail = function (email, callback) {

    db.collection('HanhKhach').aggregate(
        {$match : {"LienHe.Email" : email}},
        {$lookup: {from: 'ChiTietChuyenBay',localField: "MaDatCho",foreignField: "MaDatCho",as: "DanhSach"}}, callback);
};