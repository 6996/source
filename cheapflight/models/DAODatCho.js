/**
 * Created by bathi on 24-Oct-16.
 */
var mongoose = require('mongoose');
var collection = "DatCho";

module.exports.taoMaDatCho = function() {
    var madatcho = '';
    var key = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for (var i = 0; i < 6; i++)
        madatcho += key.charAt(Math.floor(Math.random() * key.length));

    return madatcho;
};

module.exports.layThongTinDatChoById = function(madatcho, callback) {
    db.collection(collection).findOne({MaDatCho: madatcho},
        {_id: false}, function (err, reply) {
            try {
                if (err)
                    throw err;

                callback(null, reply);
            }
            catch (err) {
                callback(err, -1);
            }
        });
};

module.exports.taoDatCho = function(callback) {
    var booking = {
        MaDatCho: this.taoMaDatCho(),
        ThoiGian: new Date().getTime(),
        TongTien: 0,
        TrangThai: 0
    };

    db.collection(collection).insertOne(booking, function (err, reply) {
        try {
            if (err)
                callback(err,null);

            if (reply.result.ok == 1)
                callback(null,{
                    MaDatCho: booking.MaDatCho
                });
         }
        catch (err) {
            callback(err,null);
        }
    });
};

module.exports.capNhatTrangThaiDatCho = function(madatcho, tongtien, callback) {
    var status = 0;
    if (tongtien > 0)
        status = 1;
    db.collection(collection).update({MaDatCho: madatcho},
        {
            $set: {
                TongTien: tongtien,
                TrangThai: status
            }
        }, function (err, reply) {
            try {
                if (err)
                    throw err;

                if (reply.result.ok == 1)
                    callback({
                        TongTien: tongtien,
                        TrangThai:status
                    });
                else
                    throw err;
            }
            catch (err) {
                console.log(err)
                callback(-1);
            }
        });
};
