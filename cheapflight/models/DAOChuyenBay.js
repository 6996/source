/**
 * Created by bathi on 24-Oct-16.
 */

var cheerio = require('cheerio');
var Nightmare = require('nightmare');

var async = require("async");

module.exports.layDSSanBayDi = function(callback) {
    db.collection("SanBay").aggregate(callback);
};

module.exports.layDSSanBayDenTuSanBayDi = function(sanbaydi, callback) {
    db.collection('ChuyenBay').aggregate(
        {$match: {SanBayDi: sanbaydi}},

        {$lookup: {from: 'SanBay', localField: "SanBayDen", foreignField: "MaSanBay", as: "SanBay"}},
        {$unwind: "$SanBay"},
        {
            $group: {
                _id: {MaSanBay: "$MaSanBay", SanBay: "$SanBay"}
            }
        }, function (err, reply) {
            try {
                if (err) {
                    throw err;
                }
                var result = [];
                for (var i = 0; i < reply.length; i++) {
                    result.push({'MaSanBay': reply[i]._id.SanBay.MaSanBay, 'Ten': reply[i]._id.SanBay.Ten});
                }
                callback(null, result);
            }
            catch (err) {
                callback(err, null);
            }
        });
};

module.exports.TimChuyenBay = function(query, thoigiandi, nguoilon, treem, sosinh,tiente, callback) {
    db.collection('ChuyenBay').find(query, { _id : false }).toArray(function (err, reply) {
        try {
            if (err)
                throw err;

            var result = [];
            for (var i = 0; i < reply.length; i++) {
                if (0 <= (reply[i].ThoiGianDi - parseInt(thoigiandi)) && (reply[i].ThoiGianDi - parseInt(thoigiandi) <= 86400)) {
                    if (reply[i].SoLuongGhe - reply[i].GheDaDat >= nguoilon + treem + sosinh) {
                        result.push(reply[i]);
                    }
                }
            }
            callback(null, result);
        }
        catch  (err) {
            callback(err, null);
        }
    });
};

module.exports.TimNhieuChang = function(query, thoigiandi, nguoilon, treem, sosinh,tiente, callback) {
    db.collection('ChuyenBay').find(query, { _id : false }).toArray(function (err, reply) {
        try {
            if (err)
                throw err;
            var result = [];
            for (var i = 0; i < reply.length; i++) {
                if (0 <= reply[i].ThoiGianDi - parseInt(thoigiandi) && reply[i].ThoiGianDi - parseInt(thoigiandi) <= 86400) {
                    if (reply[i].SoLuongGhe - reply[i].GheDaDat >= nguoilon + treem + sosinh) {
                        result.push(reply[i]);
                    }
                }
            }
            callback(null, result);
        }
        catch  (err) {
            callback(err, null);
        }
    });
};

module.exports.capNhatGheDaDat = function(flights, songuoi, callback) {
    for(var i=0;i<flights.length;i++) {
        db.collection('ChuyenBay').update({
            Ma: flights[i].MaChuyenBay,
            ThoiGianDi: Number(flights[i].Ngay) ,
            Hang: flights[i].Hang,
            MucGia: flights[i].MucGia
        }, {$inc: {GheDaDat:  songuoi}}, function (err, reply) {
            console.log(reply);
        });
    }

    callback();
};

module.exports.RutTrich = function(query,sanbaydi,sanbayden, thoigiandi, nguoilon, treem, sosinh,tiente, callback){
    var strdate = new Date((thoigiandi + 25200)*1000);
    strdate = strdate.toISOString().substring(0, 10);
    var url = "http://www.dohop.vn/flights/" + sanbaydi + "/" + sanbayden + "/" + strdate.toString() + "/"
        +nguoilon.toString() + "-adults";
    if(treem !=0 || sosinh != 0)
    {
        url = url + "children";

        for(var i = 0; i < treem;i++)
        {
            url = url + "-8";
        }
        for(var j in sosinh)
        {
            url = url + "-0";
        }
    }
    var flights = [];
    var nightmare = new Nightmare({ show: true });
        nightmare
            .goto(url)
            .wait(7000)
            .click('.Itinerary__arrow')
            .evaluate(function () {
                var page = document.querySelector('.Results__itineraries').innerHTML;
                return page
            })
            .end()
            .then(function (result) {
                try {
                    var $ = cheerio.load(result);
                    $('.Itinerary').each(function(i, element){
                        var d = $(this);
                        var Hang = d.children('.Itinerary__clickable').children('.Itinerary__main').children('.Itinerary__header')
                            .children('.Itinerary__airlineInfo').children().eq(0).text();
                        var SanBayDi = sanbaydi;
                        var SanBayDen = sanbayden;
                        var GioDi = d.children('.Itinerary__clickable').children('.Itinerary__main')
                            .children('.Itinerary__route').children('.Itinerary__cell').eq(1).text();
                        var GioDen = d.children('.Itinerary__clickable').children('.Itinerary__main')
                            .children('.Itinerary__route').children('.Itinerary__cell').eq(4).text();
                        var ThoiGianBay = d.children('.Itinerary__clickable').children('.Itinerary__main')
                            .children('.Itinerary__route').children('.Itinerary__cell').eq(6).text();
                        var Gia = d.children('.Itinerary__clickable').children('.MatchMedia')
                            .children('.ItineraryPrice').children('.MatchMedia').children()
                            .children('.ItineraryPrice__content').children('.ItineraryPrice__fareInfo').children().eq(0).text();
                        var TenTrangWeb = d.children('.Itinerary__clickable').children('.MatchMedia')
                            .children('.ItineraryPrice').children('.MatchMedia').children()
                            .children('.ItineraryPrice__content').children('.ItineraryPrice__fareInfo').children().next().eq(0).text();
                        var Url = d.children('.Itinerary__clickable').children('.MatchMedia')
                            .children('.ItineraryPrice').children('.MatchMedia').children()
                            .children('.ItineraryPrice__content').children('.ItineraryPrice__fareInfo').children().next().eq(0).attr('href');
                        var ThoiGianDi = thoigiandi;

                        var metadata = {
                            Hang: Hang,
                            SanBayDi: SanBayDi,
                            SanBayDen: SanBayDen,
                            ThoiGianDi: strdate,
                            GioDi: GioDi,
                            GioDen: GioDen,
                            ThoiGianBay: ThoiGianBay,
                            Gia: Gia,
                            TenTrangWeb: TenTrangWeb,
                            Url: "http://www.dohop.vn"+ Url
                        };
                        flights.push(metadata);
                    });
                    console.log(flights.length);
                    callback(null, flights);
                } catch (err) {
                    // Call callback with error
                    return callback(err);
                }
            });



};