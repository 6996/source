/**
 * Created by bathi on 24-Oct-16.
 */

    var mongoose = require('mongoose');
    var collection = "ChiTietChuyenBay";

module.exports.layChuyenBay = function(madatcho, callback) {
    db.collection(collection).findOne({MaDatCho: madatcho}, function (err, chuyenbay) {
            try {
                if (err)
                    throw err;
                callback(chuyenbay);
            }
            catch (err) {
                callback(-1);
            }
        });
};

module.exports.themChiTietChuyenBay = function(madatcho, machuyenbay, thoigian, hang, mucgia, giaban,callback) {
    var details = {
        MaDatCho: madatcho,
        MaChuyenBay: machuyenbay,
        Ngay: thoigian,
        Hang: hang,
        MucGia: mucgia,
        GiaBan: giaban
    };

    db.collection(collection).insert(details, function (err, result) {callback(null,result);});
};


module.exports.layHanhKhach = function(madatcho, callback) {
    db.collection('HanhKhach').findOne({MaDatCho: madatcho},
        {_id: false, HanhKhach: true, LienHe:true}, function (err, hanhkhach) {
            try {
                if (err)
                    throw err;

                callback(hanhkhach);
            }
            catch (err) {
                callback(-1);
            }
        });
};

module.exports.layHanhKhachvaChuyenBay = function(madatcho, callback) {
    db.collection('HanhKhach').aggregate({$match :{MaDatCho: madatcho}},
    {$lookup:{from:'ChiTietChuyenBay',localField:"MaDatCho",foreignField:"MaDatCho",as:"ChuyenBay"}}, function (err, reply) {
            try {
                if (err)
                    throw err;
                    callback(null,reply);
            }
            catch (err) {
                callback(err, -1);
            }
        });
};

module.exports.themHanhKhach = function(madatcho, hanhkhach, lienhe, callback) {
    db.collection('HanhKhach').update({MaDatCho: madatcho},
        {MaDatCho: madatcho, HanhKhach: hanhkhach, LienHe:lienhe},{ upsert : true }, function (err, reply) {
            try {
                if (err)
                    throw err;

                if (reply.result.ok == 1)
                    callback({
 						data:reply,
                        success: true
                    });
                else
                    callback(-1);
            }
            catch (err) {
                callback(-1);
            }
        });
};