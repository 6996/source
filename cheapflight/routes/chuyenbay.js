/**
 * Created by bathi on 24-Oct-16.
 */
var express = require('express');
var router = express.Router();
var DAOChuyenBay = require('../models/DAOChuyenBay');


router.get('/sanbaydi', function(req, res, next) {
    DAOChuyenBay.layDSSanBayDi(function (err, DSSanBayDi) {
        if (err) {
            res.status(404).send({ error: "Khong tim thay du lieu san bay" });
        }else
            res.json(DSSanBayDi);
    });
});

router.get('/sanbayden', function(req, res, next) {
    if(req.query.sanbaydi != undefined)
    {
        var sanbaydi = (req.query.sanbaydi);
        if ( validator.isAlpha (sanbaydi))
        {
            DAOChuyenBay.layDSSanBayDenTuSanBayDi(sanbaydi, function(err, DSSanBayDen) {
                if (err) {
                    res.status(404).send({ error: "Khong tim thay du lieu san bay" });
                }
                else
                    res.json(DSSanBayDen);
            });
        }
        else
        {
            res.statusCode = 400;
            return res.json({
                error: 'Error 400: Ma San bay di incorrect format'
            });
        }
    }
    else
    {
        DAOChuyenBay.layDSSanBaydi(function (err, DSSanBayDi) {
            if (err) {
                res.status(406).send({ error: "Khong truyen san bay di" });
            }else
                res.json(DSSanBayDi);
        });
    }
});
//
router.get('/', function(req, res, callback) {

    var query = {};
    query["$and"]=[];
    var sanbaydi="";
    var sanbayden="";
    var nguoilon=0;
    var treem=0;
    var sosinh=0;
   // var hang="";
    var tiente="VND";
    var thoigiandi=0;
    if(req.query.sanbaydi != undefined) {
        sanbaydi = req.query.sanbaydi;
        if (validator.isAlpha(sanbaydi))
        {
            query["$and"].push({'SanBayDi':sanbaydi});
        }
        else
        {
            res.statusCode = 400;
            return res.json({
                error: 'Error 400: San bay di incorrect format'
            });
        }
    }
    if(req.query.sanbayden != undefined)
    {
        sanbayden = req.query.sanbayden;
        if (validator.isAlpha(sanbayden))
        {
            query["$and"].push({'SanBayDen':sanbayden});
        }
        else
        {
            res.statusCode = 400;
            return res.json({
                error: 'Error 400: San bay den incorrect format'
            });
        }
    }
    if(req.query.nguoilon != undefined)
    {
        nguoilon = parseInt(req.query.nguoilon);
        if ( nguoilon > 10 )
        {
            res.statusCode = 400;
            return res.json({
                error: 'Error 400: So nguoi lon vuot qua dinh muc'
            });
        }
    }
    if(req.query.treem != undefined)
    {
        treem = parseInt(req.query.treem);
        if ( treem > 10 )
        {
            res.statusCode = 400;
            return res.json({
                error: 'Error 400: So tre em vuot qua dinh muc'
            });return;
        }
    }
    if(req.query.sosinh != undefined)
    {
        sosinh = parseInt(req.query.sosinh);
        if ( sosinh > 10 )
        {
            res.statusCode = 400;
            return res.json({
                error: 'Error 400: So tre so sinh vuot qua dinh muc'
            });
        }
    }
    // if(req.query.hang != undefined)
    // {
    //     hang = req.query.hang;
    //     query["$and"].push({'Hang':hang});
    // }
    if(req.query.tiente != undefined)
    {
        tiente = req.query.tiente;
    }
    if(req.query.ngaydi != undefined)
    {
        thoigiandi = req.query.ngaydi;
    }
    DAOChuyenBay.TimChuyenBay(query, thoigiandi, nguoilon, treem, sosinh,tiente,function(err, flight) {
        if (err) {
            res.statusCode = 500;
            return res.json({
                error : 'Error 500: Server error.'
            });
        }
        res.json(flight);
    });
});

router.post ('/nhieuchang', function (req,res) {

    var nguoilon=0;
    var treem=0;
    var sosinh=0;
    var hang="";
    var tiente="VND";
    if(req.query.nguoilon != undefined)
    {
        nguoilon = parseInt(req.query.nguoilon);
        if ( nguoilon > 10 )
        {
            res.statusCode = 400;
            return res.json({
                error: 'Error 400: So nguoi lon vuot qua dinh muc'
            });
        }
    }
    if(req.query.treem != undefined)
    {
        treem = parseInt(req.query.treem);
        if ( treem > 10 )
        {
            res.statusCode = 400;
            return res.json({
                error: 'Error 400: So tre em vuot qua dinh muc'
            });return;
        }
    }
    if(req.query.sosinh != undefined)
    {
        sosinh = parseInt(req.query.sosinh);
        if ( sosinh > 10 )
        {
            res.statusCode = 400;
            return res.json({
                error: 'Error 400: So tre so sinh vuot qua dinh muc'
            });
        }
    }
    if(req.body.tiente != undefined)
    {
        tiente = req.body.tiente;
    }
    var result=[];
    function NhieuChang ( i , result){
        if ( i < req.body.changbay.length )
        {
            var sanbayden="";
            var sanbaydi="";
            var thoigiandi;
            var query = {};
            query["$and"]=[];

            if(req.body.changbay[i].sanbaydi != undefined) {
                sanbaydi = req.body.changbay[i].sanbaydi;
                query["$and"].push({'SanBayDi':sanbaydi});
            }
            if(req.body.changbay[i].sanbayden != undefined)
            {
                sanbayden = req.body.changbay[i].sanbayden;
                query["$and"].push({'SanBayDen':sanbayden});

            }
            if(req.body.changbay[i].thoigiandi != undefined)
            {
                thoigiandi = req.body.changbay[i].thoigiandi
            }
            DAOChuyenBay.TimNhieuChang(query, thoigiandi, nguoilon, treem, sosinh,tiente,
                function(err, flight) {
                    if (err) {
                        res.statusCode = 500;
                        return res.json({
                            error : 'Error 500: Server error.'
                        });
                    }
                    result.push(flight);
                    NhieuChang(i+1, result);
                    return;
                });
        }
        else {
            res.json(result);
        }

    }
    NhieuChang(0,result);
});

router.get('/ruttrich', function(req, res, callback) {
    var query = {};

    query["$and"]=[];
    var sanbaydi="";
    var sanbayden="";
    var nguoilon=0;
    var treem=0;
    var sosinh=0;
    var hang="";
    var uutien=false;
    var tiente="VND";
    var thoigiandi=0;

    if(req.query.sanbaydi != undefined)
    {
        sanbaydi = req.query.sanbaydi;
        query["$and"].push({'SanBayDi':sanbaydi});
    }
    if(req.query.sanbayden != undefined)
    {
        sanbayden = req.query.sanbayden;
        query["$and"].push({'SanBayDen':sanbayden});
    }
    if(req.query.nguoilon != undefined)
    {
        nguoilon = parseInt(req.query.nguoilon);
        if ( nguoilon > 10 )
        {
            res.statusCode = 400;
            return res.json({
                error: 'Error 400: So nguoi lon vuot qua dinh muc'
            });
        }
    }
    if(req.query.treem != undefined)
    {
        treem = parseInt(req.query.treem);
        if ( treem > 10 )
        {
            res.statusCode = 400;
            return res.json({
                error: 'Error 400: So tre em vuot qua dinh muc'
            });return;
        }
    }
    if(req.query.sosinh != undefined)
    {
        sosinh = parseInt(req.query.sosinh);
        if ( sosinh > 10 )
        {
            res.statusCode = 400;
            return res.json({
                error: 'Error 400: So tre so sinh vuot qua dinh muc'
            });
        }
    }
    if(req.query.hang != undefined)
    {
        hang = req.query.hang;
        query["$and"].push({'Hang':hang});
    }
    if(req.query.tiente != undefined)
    {
        tiente = req.query.tiente;
    }
    if(req.query.ngaydi != undefined)
    {
        thoigiandi = parseInt(req.query.ngaydi);
    }
    DAOChuyenBay.RutTrich(query,sanbaydi,sanbayden, thoigiandi, nguoilon, treem, sosinh,tiente,function(err, flight) {
        if (err) {
            res.statusCode = 500;
            return res.json({
                error : 'Error 500: Server error.'
            });
        }
        res.json(flight);
    });
});

module.exports = router;