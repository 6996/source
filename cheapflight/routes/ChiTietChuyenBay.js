/**
 * Created by bathi on 24-Oct-16.
 */
var express = require('express');
var router = express.Router();
var DAOChiTietChuyenBay = require('../models/DAOChiTietChuyenBay');

router.get('/:madatcho/chuyenbay', function(req, res) {
    DAOChiTietChuyenBay.layChuyenBay(req.params.madatcho, function (result) {
        if (result == -1) {
            res.statusCode = 500;
            return res.json({
                error: 'Error 500: Server error.'
            });
        }

        res.json(result);
    });
});

router.get('/:madatcho/hanhkhach', function(req, res) {
    DAOChiTietChuyenBay.layHanhKhach(req.params.madatcho, function (result) {
        if (result == -1) {
            res.statusCode = 500;
            return res.json({
                error: 'Error 500: Server error.'
            });
        }

        res.json(result);
    });
});

router.post('/:madatcho/chuyenbay', function(req, res) {
    console.log(req.body);
    if (!req.params.madatcho || !req.body.hasOwnProperty('MaChuyenBay') ||
        !req.body.hasOwnProperty('ThoiGian') || !req.body.hasOwnProperty('Hang') ||
        !req.body.hasOwnProperty('MucGia') || !req.body.hasOwnProperty('GiaBan')) {
        res.statusCode = 400;
        return res.json({
            error: 'Error 400: Syntax incorrect.'
        });
    }
    if ( req.params.madatcho.toString().length != 6 || !validator.isAlphanumeric(req.params.madatcho))
    {
        return res.json({
            error: 'Error 400: Ma dat cho incorrect format.'
        });
    }
    else {
        DAOChiTietChuyenBay.themChiTietChuyenBay(req.params.madatcho, req.body.MaChuyenBay, Date.parse(req.body.ThoiGian)/1000, req.body.Hang,
            req.body.MucGia, req.body.GiaBan, function (err,result) {
                if (err) {
                    res.statusCode = 500;
                    return res.json(err);
                }
                else {
                    res.statusCode = 201;
                    res.json(result);
                }
            });
    }
});

router.post('/:madatcho/hanhkhach', function(req, res) {
    if (!req.body.hasOwnProperty('HanhKhach')) {
        res.statusCode = 400;
        return res.json({
            error: 'Error 400: Syntax incorrect.'
        });
    }
    if ( req.params.madatcho.toString().length != 6 || !validator.isAlphanumeric(req.params.madatcho))
    {
        return res.json({
            error: 'Error 400: Ma dat cho incorrect format.'
        });
    }
    else {
        DAOChiTietChuyenBay.themHanhKhach(req.params.madatcho, req.body.HanhKhach,req.body.LienHe, function (result) {
            if (result == -1) {
                res.statusCode = 500;
                return res.json({
                    error: 'Error 500: Server error.'
                });
            }
            res.statusCode = 201;
            res.json(result);
        });
    }
});
module.exports = router;
