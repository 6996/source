/**
 * Created by Thinh on 10/25/2016.
 */

var express = require('express');
var router = express.Router();
var DAOAdmin = require('../models/DAOAdmin');


///lấy tất cả dữ liệu chuyến bay
router.get('/chuyenbay', function(req, res) {

    DAOAdmin.layTatCaChuyenBay(function(err, chuyenbay){
        if(err){
            res.status(err.status).send(err.message);
            throw err;
        }
        res.status(200).json(chuyenbay);
    });
});
///thêm dữ liệu chuyến bay
router.post('/chuyenbay', function(req, res){
    var chuyenbay = req.body;
    if ( validator.isAlphanumeric (chuyenbay.Ma) &&
         validator.isAlpha (chuyenbay.SanBayDi) &&
         validator.isAlphanumeric (chuyenbay.SanBayDen) &&
         validator.isNumeric (chuyenbay.ThoiGianDi.toString()) &&
         validator.isAlphanumeric (chuyenbay.Hang) &&
         validator.isAlphanumeric (chuyenbay.MucGia) &&
         validator.isNumeric (chuyenbay.SoLuongGhe.toString()) &&
         validator.isNumeric (chuyenbay.GiaBan.toString()) &&
         validator.isNumeric (chuyenbay.GheDaDat.toString()) )
    {
        DAOAdmin.themChuyenBay(chuyenbay, function(err, chuyenbay){
            if(err){
                res.status(err.status).send(err.message);
                throw err;
            }
            res.status(201).send(chuyenbay);
        });
    }
    else {
        res.statusCode = 400;
        return res.json({
            error: 'Error 400: Input data incorrect format.'
        });
    }

});

/// sửa dữ liệu chuyến bay
router.put('/chuyenbay/:_id', function(req, res, next){

    var id = req.params._id;
    var chuyenbay = req.body;
    if(validator.isAlphanumeric(id) && id.toString().length > 0 ) {
        if ( validator.isAlphanumeric (chuyenbay.Ma) &&
            validator.isAlpha (chuyenbay.SanBayDi) &&
            validator.isAlpha (chuyenbay.SanBayDen) &&
            validator.isNumeric (chuyenbay.ThoiGianDi.toString()) &&
            validator.isAlphanumeric (chuyenbay.Hang) &&
            validator.isAlpha (chuyenbay.MucGia) &&
            validator.isNumeric (chuyenbay.SoLuongGhe.toString()) &&
            validator.isNumeric (chuyenbay.GiaBan.toString()) &&
            validator.isNumeric (chuyenbay.GheDaDat.toString()) ){
            DAOAdmin.capnhatChuyenBay(id, chuyenbay, {}, function (err, chuyenbay) {
                if (err) {
                    res.status(err.status).send(err.message);
                    throw err;
                }
                res.status(200).send(chuyenbay);
            });
        } else {
            res.statusCode = 400;
            return res.json({
                error: 'Error 400: Input data incorrect format.'
            });
        }
    }
    else {
        res.status(404).send({error: "The resource contains only letters and numbersc."});
    }
});

///xóa dữ liệu chuyến bay
router.delete('/chuyenbay/:_id', function(req, res){
    var id = req.params._id;
    if(validator.isAlphanumeric(id) && id.toString().length > 0 ) {
        DAOAdmin.xoaChuyenBay(id, function (err, chuyenbay) {
            if (err) {
                res.status(err.status).send(err.message);
                throw err;
            }
            res.status(204).send(chuyenbay);
        });
    }
    else {
        res.status(404).send({error: "The resource contains only letters and numbersc."});
    }
});

///thêm dữ liệu sân bay
router.post('/sanbay', function(req, res, next){
    var sanbay = req.body;
    if ( sanbay.Ten.toString().length > 0 &&
        sanbay.MaSanBay.toString().length > 0 && validator.isAlpha ( sanbay.MaSanBay )){
        DAOAdmin.themSanbay(sanbay, function (err, sanbay) {
            if (err) {
                res.status(err.status).send(err.message);
                throw err;
            }
            res.status(201).send(sanbay);
        });
    }
    else {
        res.statusCode = 400;
        return res.json({
            error: 'Error 400: Input data incorrect format.'
        });
    }
});
/// sửa dữ liệu sân bay
router.put('/sanbay/:MaSanBay', function(req, res, next){
    var id = req.params.MaSanBay;
    var sanbay = req.body;
    if(validator.isAlphanumeric(id) && id.toString().length > 0 ) {
        if ( sanbay.Ten.toString().length > 0 &&
            sanbay.MaSanBay.toString().length > 0 && validator.isAlpha ( sanbay.MaSanBay ))
        {
            DAOAdmin.capnhatSanbay(id, sanbay, {},function (err, sanbay) {
                if (err) {
                    res.status(err.status).send(err.message);
                    throw err;
                }
                res.status(200).send(sanbay);
            });
        }
        else {
            res.statusCode = 400;
            return res.json({
                error: 'Error 400: Input data incorrect format.'
            });
        }
    }
    else {
        res.status(404).send({error: "The resource contains only letters and numbersc."});
    }
});

///xóa dữ liệu san bay
router.delete('/sanbay/:_masanbay', function(req, res){
    var masanbay = req.params._masanbay;
    if(validator.isAlphanumeric(masanbay))
    {
        DAOAdmin.xoaSanbay(masanbay, function (err, sanbay) {
            if (err) {
                res.status(err.status).send(err.message);
                throw err;
            }
            res.status(204).send(sanbay);
        });
    }
    else {
        res.status(404).send({error: "The resource contains only letters and numbersc."});
    }
});

///sửa thông tin hành khách

router.put('/hanhkhach', function(req, res) {
    var hanhkhach = req.body;
    if(hanhkhach.toString().length > 0)
    {
        DAOAdmin.capnhatThongtinHanhKhach(hanhkhach, {}, function (err, hanhkhach) {
            if (err) {
                res.status(err.status).send(err.message);
                throw err;
            }
            res.status(200).send(hanhkhach);
        });
    }
    else {
        res.status(404).send({error: "The resource contains only letters and numbersc."});
    }
});

///lấy thông tin hành khách trên chuyến bay
router.get('/hanhkhach/:_machuyenbay', function(req, res) {

    var id = req.params._machuyenbay;
    if(validator.isAlphanumeric(id)) {
        DAOAdmin.layHanhKhachtrenChuyenBay(id, function (err, hanhkhach) {
            if (err) {
                res.status(err.status).send(err.message);
                throw err;
            }
            res.status(200).json(hanhkhach);
        });
    }
    else {
        res.status(404).send({error: "The resource contains only letters and numbersc."});
    }
});

///Lấy thông tin chuyến bay đã đặt từ email
router.get('/hanhkhach/danhsachchuyenbay/:_email', function(req, res, next) {
    var email = req.params._email;
    if(validator.isEmail(email))
    {
        DAOAdmin.layDanhSachChuyenBaytuEmail(email, function(err, hanhkhach){
            if(err){
                res.status(err.status).send(err.message);

            }
            res.status(200).json(hanhkhach);
        });
    }
    else {
        res.status(404).send({error: "The specified resource does not exist."});
    }
});

module.exports = router;