/**
 * Created by THINH on 11/2/2016.
 */
var express = require('express');
var router = express.Router();
var DAOAutho = require('../models/DAOAutho');



// var user = {
//     username: 'trehamvui',
//     password: 'thinhdeptrai'
// };

router.post('/sessions/create', function(req, res) {


    if (!req.body.username || !req.body.password) {
        return res.status(400).send("You must send the username and the password");
    }

    //Tìm người thân
    DAOAutho.findUser(req.body,function(err, result){
        if(err){
            res.status(err.status).send(err.message);
            throw err;
        }
        if(result != null) {
            res.status(201).send({
                token: DAOAutho.createToken(result)
            });
        }
        if(result == null){
           return res.status(400).send("You must send the username and the password");
        }
    });

});





module.exports = router;