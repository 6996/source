/**
 * Created by bathi on 24-Oct-16.
 */
var waterfall = require('async-waterfall');
var express = require('express');
var router = express.Router();
var DAODatCho = require('../models/DAODatCho');
var DAOChiTietChuyenBay = require('../models/DAOChiTietChuyenBay');
var DAOChuyenBay = require('../models/DAOChuyenBay');

router.get('/:madatcho', function(req, res, next) {
    if ( validator.isAlphanumeric (req.params.madatcho))
    {
        DAODatCho.layThongTinDatChoById(req.params.madatcho, function (err, result) {
            if (result == -1) {
                res.statusCode = 500;
                return res.json({
                    error: 'Error 500: Server error.'
                });
            }
            res.json(result);
        });
    }
    else
    {
        res.statusCode = 400;
        return res.json({
            error: 'Error 400: Ma dat cho incorrect format'
        });
    }

});

router.post('/', function(req, res) {
    waterfall([
        function taodatcho(callback) {
            DAODatCho.taoDatCho(function (err, result) {
                if (err!=null) {
                    res.statusCode = 500;
                    callback(err, null);
                    return;
                }
                var madatcho = result.MaDatCho;
                callback(null, madatcho);
            });
        },
        function themchitiet(madatcho, callback) {
            if ( req.body.MaChuyenBay.toString().length != 0 && validator.isAlphanumeric(req.body.MaChuyenBay) &&
                req.body.ThoiGian.toString().length !=0 && validator.isNumeric(req.body.ThoiGian.toString()) &&
                req.body.Hang.toString().length !=0 && validator.isAlpha(req.body.Hang) &&
                req.body.MucGia.toString().length !=0 && validator.isAlpha(req.body.MucGia) &&
                req.body.GiaBan.toString().length != 0 && validator.isNumeric(req.body.GiaBan.toString()) )
            {
                DAOChiTietChuyenBay.themChiTietChuyenBay(madatcho, req.body.MaChuyenBay, (req.body.ThoiGian), req.body.Hang,
                    req.body.MucGia, req.body.GiaBan, function (err1, result1) {
                        if (err1) {
                            callback(err1, null);
                        }
                        else {
                            callback(null, result1);
                        }
                    });
            }
            else {
                res.statusCode = 400;
                return res.json({
                    error: 'Error 400: Format Data incorrect.'
                });
            }
        }], function (err, result) {
        if (err) {
            res.statusCode = 500;
            return res.json(err);
        }
        else {
            res.statusCode = 201;
            res.json(result);
        }

    })
});

router.post('/nhieuchang', function(req, res) {
    waterfall([
        function taodatcho(callback) {
            DAODatCho.taoDatCho(function (err, result) {
                if (err!=null) {
                    res.statusCode = 500;
                    callback(err, null);
                    return;
                }
                var madatcho = result.MaDatCho;
                callback(null, madatcho);
            });
        },
        function themchitiet(madatcho, callback) {
            if ( req.body.hasOwnProperty('changbay') && req.body.changbay.length > 0 )
            {
                var result2=[];
                function NhieuChang (i,result2)
                {
                    if ( i < req.body.changbay.length ) {
                        if ( req.body.changbay[i].MaChuyenBay.toString().length != 0 && validator.isAlphanumeric(req.body.changbay[i].MaChuyenBay) &&
                        req.body.changbay[i].ThoiGian.toString().length != 0 && validator.isNumeric(req.body.changbay[i].ThoiGian.toString()) )
                        {
                            DAOChiTietChuyenBay.themChiTietChuyenBay(madatcho, req.body.changbay[i].MaChuyenBay, (req.body.changbay[i].ThoiGian),
                                req.body.changbay[i].Hang,
                                req.body.changbay[i].MucGia, req.body.changbay[i].GiaBan, function (err1, result1) {
                                    if (err1) {
                                        callback(err1, null);
                                        return;
                                    }
                                    else {
                                        result2.push(result1);
                                        NhieuChang(i + 1, result2);
                                        return;
                                    }
                                });
                        }
                        else {
                            res.statusCode = 400;
                            return res.json({
                                error: 'Error 400: Input data incorrect format.'
                            });
                        }
                    }
                    else {
                        callback(null,result2);
                    }
                }
                NhieuChang(0,result2);
            }
            else {
                res.statusCode = 400;
                return res.json({
                    error: 'Error 400: Input data lost.'
                });
            }

        }], function (err, result) {
        if (err) {
            res.statusCode = 500;
            return res.json(err);
        }
        else {
            res.statusCode = 201;
            var temp =[];
            for (var i = 0 ; i < result.length ; i++ )
            {
                temp.push(result[i].ops);
            }
            res.json(temp);
        }
    })
});



router.put('/:madatcho', function(req, res) {
    if ( req.params.madatcho.toString().length === 6 && validator.isAlphanumeric(req.params.madatcho))
    {
        DAOChiTietChuyenBay.layHanhKhachvaChuyenBay(req.params.madatcho, function (err,callback) {
            if (err) {
                res.statusCode = 500;
                return res.json({
                    error: 'Error 500: Server error 1.'
                });
            }
            console.log(callback[0]);
            var totalCost = req.body.TongTien;
            var number = callback[0].HanhKhach.length;
            var ChuyenBay = callback[0].ChuyenBay;
            DAODatCho.capNhatTrangThaiDatCho(req.params.madatcho, totalCost, function (result) {
                if (result == -1) {
                    res.statusCode = 500;
                    return res.json({
                        error: 'Error 500: Server error.'
                    });
                }
                DAOChuyenBay.capNhatGheDaDat(ChuyenBay, number, function () {
                    res.statusCode = 202;
                    res.json(result);
                });
            });

        });
    }
    else
    {
        res.statusCode = 400;
        return res.json({
            error: 'Error 400: Input data fornmat incorrect.'
        });
    }

});

module.exports = router;